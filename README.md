# Run gollum and plantuml-server on local machine directly

## plantuml-server

https://github.com/plantuml/plantuml-server

```
$ brew cask install java
$ brew install maven
$ mvn jetty:run
```

Plant UML Server runs at http://localhost:8080/.

Base URL is http://localhost:8080/plantuml/.

See also http://localhost:8080/plantuml/png.

## gollum

Assuming `ruby` is installed by `rvm`, `rbenv` or similar tool.

```
$ gem install gollum
$ cd ~/my-gollum-wiki
$ git init
$ cat <<EOF > config.rb
Gollum::Filter::PlantUML.configure do |config|
  config.url = "http://localhost:8080/plantuml/png"
end
EOF
$ gollum --host=127.0.0.1 --config=config.rb .
```

Open http://localhost:4567/.

https://github.com/gollum/gollum/wiki/Custom-PlantUML-Server

_By default gollum-lib expects the PlantUML server to be located on http://localhost:8080/plantuml/png._
https://github.com/gollum/gollum/wiki/Custom-PlantUML-Server#configuring-a-plantuml-server

Now local gollum uses local plantuml server instead of https://www.plantuml.com.
